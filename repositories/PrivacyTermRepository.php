<?php

namespace Viamage\GDPRHelper\Repositories;

use October\Rain\Database\Collection;
use Viamage\GDPRHelper\Models\PrivacyTerm;

/**
 * Class PrivacyTermRepository
 * @package Viamage\GDPRHelper\Repositories
 */
class PrivacyTermRepository
{
    /**
     * @return Collection
     */
    public function getVisible(): Collection
    {
        return PrivacyTerm::where('is_displayed', true)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'asc')
            ->rememberForever('vm_privacy_terms')
            ->get();
    }
}