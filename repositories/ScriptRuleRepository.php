<?php

namespace Viamage\GDPRHelper\Repositories;

use October\Rain\Database\Collection;
use Viamage\GDPRHelper\Models\ScriptRule;

/**
 * Class ScriptRuleRepository
 * @package Viamage\GDPRHelper\Repositories
 */
class ScriptRuleRepository
{
    /**
     * @return Collection|ScriptRule[]
     */
    public function getVisible(): Collection
    {
        return ScriptRule::where('is_displayed', true)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'asc')
            ->rememberForever('vm_script_rules')
            ->get();
    }

    /**
     * @param array $agreed
     * @return Collection|ScriptRule[]
     */
    public function getAgreed(array $agreed): Collection
    {
        return ScriptRule::whereIn('slug', $agreed)->get();
    }
}