# GDPR Helper

This is OctoberCMS plugin that may help with making your website GDPR compliant.

Requirements:

- October Ajax Framework enabled. and jQuery. 

(PRs to remove that dependency are welcome)

### Installation

Clone the repo to /plugins/viamage/gdprhelper and run october:up

### Privacy Terms

![privacy](https://i.viamage.com/jz/screen-2018-05-28-13-01-24.png)

This section is for creating Privacy Policy for your website.

It consists of some preseeded point that your policy should describe in order to be compliant with GDPR. 

Use **[vm_privacy_terms]** component to display it. 


### Script Rules

![script](https://i.viamage.com/jz/screen-2018-05-28-13-02-26.png)

This section allows you to create on-consent rules for loading JavaScript snippets. 

Example would be some specialized analytics JS that should be loaded only if visitor opts in. Component collects user consents on first visit:

![consents](https://i.viamage.com/jz/screen-2018-05-28-12-10-48.png)

saves them in special Cookie and pushes JS snippets into `<div id="cookies_scripts"></div>` for agreed entries only. 

[Click to watch Demo movie](http://uploads.keios.eu/video/scriptrules-2018-05-28_12.37.58.html)

### Components

There are two: 

```
vm_privacy_terms
vm_cookie_warning
```

First is for displaying Privacy Policy, second for cookie warning. Feel free to overwrite the partials in your theme. 

### Caching

Use Redis or something. In this plugin we do **cache**, look:

![cache](https://i.viamage.com/jz/screen-2018-05-28-14-39-10.png)

### TODO

#### Medium Priority
- Translatable behavior
- More pre-filled entries in Privacy Terms

#### Low Priority
- Replacing jQuery in cookie.js with clever vanillia fades.

#### Ideas
- Some widget to change scripts settings? (Right now probably guide your user on how to remove cookies).
- Optional Extending RainLab.User and storing opt-ins in DB? 

 
