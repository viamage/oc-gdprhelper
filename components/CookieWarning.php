<?php namespace Viamage\GDPRHelper\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use Cookie;
use Illuminate\Http\Response;
use Viamage\GDPRHelper\Models\ScriptRule;
use Viamage\GDPRHelper\Repositories\ScriptRuleRepository;

/**
 * Class CookieWarning
 * @package Viamage\GDPRHelper\Components
 */
class CookieWarning extends ComponentBase
{

    /**
     * @var ScriptRuleRepository
     */
    private $scriptRuleRepository;

    /**
     * CookieWarning constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->scriptRuleRepository = \App::make(ScriptRuleRepository::class);
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'viamage.gdprhelper::lang.components.cookiewarning.name',
            'description' => 'viamage.gdprhelper::lang.components.cookiewarning.description',
        ];
    }

    /**
     *
     */
    public function onRun()
    {
        $this->page['cookie_rules'] = $this->scriptRuleRepository->getVisible();
    }

    /**
     *
     */
    public function onLoadCookieScripts()
    {
        if (!array_key_exists('cookiesScripts', $_COOKIE) || !$_COOKIE['cookiesScripts']) {
            return;
        }
        $consents = json_decode($_COOKIE['cookiesScripts'], true);

        /**
         * Wonder why foreach and not array_walk or something? 3x faster
         */
        $allowedScripts = [];
        foreach ($consents as $key => $agreed) {
            if ($agreed) {
                $allowedScripts[] = $key;
            }
        }

        $scriptRules = $this->scriptRuleRepository->getAgreed($allowedScripts);

        /**
         * Wonder why another foreach and not pluck -> toArray -> implode? it's 4x faster
         */
        $scripts = '';
        foreach ($scriptRules as $scriptRule) {
            if ($snippet = $scriptRule->getSnippet()) {
                $scripts .= $snippet.PHP_EOL;
            }
        }

        $this->page['cookie_scripts'] = $scripts;
    }

}