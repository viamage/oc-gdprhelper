<?php namespace Viamage\GDPRHelper\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use Viamage\GDPRHelper\Repositories\PrivacyTermRepository;

/**
 * Class PrivacyTerms
 * @package Viamage\GDPRHelper\Components
 */
class PrivacyTerms extends ComponentBase
{
    /**
     * @var PrivacyTermRepository
     */
    private $termsRepository;

    /**
     * PrivacyTerms constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->termsRepository = \App::make(PrivacyTermRepository::class);
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'viamage.gdprhelper::lang.components.privacyterms.name',
            'description' => 'viamage.gdprhelper::lang.components.privacyterms.description',
        ];
    }

    /**
     * @return void
     */
    public function onRun()
    {
        $this->page['privacy_terms'] = $this->termsRepository->getVisible();
    }

}