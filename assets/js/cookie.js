var consentIsSet = 'unknown',
  cookieBanner = '#cookieBanner',
  consentString = 'cookiesConsent=',
  scriptsString = 'cookiesScripts='

function setCookie (console_log, banner_text, consent) {
  let consents = getCookieConsents()
  console.log(console_log)
  // TODO - replace jquery with vanillia
  $(cookieBanner).text(banner_text)
  $(cookieBanner).fadeOut(50)
  let d = new Date()
  let exdays = 30 * 12 //  1 year
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000))
  let expires = 'expires=' + d.toGMTString()
  document.cookie = consentString + consent + ';' + expires + ';path=/'
  document.cookie = scriptsString + JSON.stringify(consents) + ';' + expires + ';/path=/'
  consentIsSet = consent
}

function getCookieConsents () {
  let consents = {}
  // TODO - replace jquery with vanillia
  $inputs = $('.cookieConsents')
  $inputs.each(function (i, v) {
    consents[$(this).attr('name')] = $(this).is(':checked')
  })

  return consents
}

function denyConsent () {
  setCookie('Consent denied', 'You disallowed the use of cookies.', 'false')
  // TODO - replace jquery with vanillia
  $(window).unbind('scroll')
  $('a:not(.noconsent)').unbind('click')
}

function grantConsent () {
  if (consentIsSet === 'true') return
  setCookie('Consent granted', ' ', 'true')
  doConsent()
}

function doConsent () {
  console.log('Consent was granted')
  document.getElementById('cookieBanner').style.display = 'none'
  let component = document.querySelector('#cookieBanner')
  let componentAlias = component.dataset.componentName
  let partial = componentAlias + '::cookie_scripts'
  // TODO - replace jquery with vanillia
  $.request('onLoadCookieScripts', {
    update: {
      [partial]: '#cookie_scripts'
    }
  })
}

let cookies = document.cookie.split(';')
for (let i = 0; i < cookies.length; i++) {
  let c = cookies[i].trim()
  if (c.indexOf(consentString) === 0) {
    consentIsSet = c.substring(consentString.length, c.length)
  }
}

if (consentIsSet === 'unknown') {
  // TODO - replace jquery with vanillia
  $(cookieBanner).fadeIn()
  $('.denyConsent').click(denyConsent)
  // allow re-enabling cookies
  $('#cookieBannerClose').click(grantConsent)
}
else if (consentIsSet === 'true') doConsent()