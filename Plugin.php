<?php namespace Viamage\GDPRHelper;

use Backend;
use System\Classes\PluginBase;
use Viamage\GDPRHelper\Components\CookieWarning;
use Viamage\GDPRHelper\Components\PrivacyTerms;

/**
 * GDPRHelper Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'viamage.gdprhelper::lang.plugin.name',
            'description' => 'viamage.gdprhelper::lang.plugin.description',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            PrivacyTerms::class  => 'vm_privacy_terms',
            CookieWarning::class => 'vm_cookie_warning',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'viamage.gdprhelper.privacyterms' => [
                'tab'   => 'viamage.gdprhelper::lang.plugin.name',
                'label' => 'viamage.gdprhelper::lang.permissions.privacyterms',
            ],
            'viamage.gdprhelper.scriptrules'  => [
                'tab'   => 'viamage.gdprhelper::lang.plugin.name',
                'label' => 'viamage.gdprhelper::lang.permissions.scriptrules',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return [
            'gdprhelper' => [
                'label'       => 'viamage.gdprhelper::lang.plugin.name',
                'url'         => Backend::url('viamage/gdprhelper/privacyterms'),
                'icon'        => 'icon-user-secret',
                'permissions' => ['viamage.gdprhelper.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'privacyterms' => [
                        'label'       => 'viamage.gdprhelper::lang.privacyterms.menu_label',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('viamage/gdprhelper/privacyterms'),
                        'permissions' => ['viamage.gdprhelper.privacyterms'],
                    ],
                    'scriptrules'  => [
                        'label'       => 'viamage.gdprhelper::lang.scriptrules.menu_label',
                        'icon'        => 'icon-cubes',
                        'url'         => Backend::url('viamage/gdprhelper/scriptrules'),
                        'permissions' => ['viamage.gdprhelper.scriptrules'],
                    ],
                ],
            ],
        ];
    }

}
