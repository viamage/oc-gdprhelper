<?php namespace Viamage\GDPRHelper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePrivacyTermsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_gdprhelper_privacy_terms',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->longText('content');
                $table->boolean('is_displayed')->default(false);
                $table->integer('order')->nullable();
                $table->timestamps();
            }
        );
        \DB::table('viamage_gdprhelper_privacy_terms')->insert($this->getTerms());
    }

    public function down()
    {
        Schema::dropIfExists('viamage_gdprhelper_privacy_terms');
    }

    private function getTerms()
    {
        return [
            [
                'title'        => 'Definitions',
                'content'      => '
                <p>Following terms shall have the following meanings within this Policy:</p>
<ul>
<li><strong>Cookie</strong> - means a small text file placed on your computer or device by Our Site when you visit certain parts of Our Site and/or when you use certain features of Our Site. </li>
<li><strong>Cookie Law</strong> -	means the relevant parts of the Privacy and Electronic Communications (EC Directive) Regulations 2003 and of EU Regulation 2016/679 General Data Protection Regulation (“GDPR”);</li>
</ul>
',
                'is_displayed' => true,
                'order'        => 1,
            ],
            [
                'title'        => 'Information About Us',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 2,
            ],
            [
                'title'        => 'Privacy Policy Coverage',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 3,
            ],
            [
                'title'        => 'What is Personal Data?',
                'content'      => '<p>Personal data is defined by the General Data Protection Regulation (EU Regulation 2016/679) (the “GDPR”) as ‘any information relating to an identifiable person who can be directly or indirectly identified in particular by reference to an identifier’.</p>
<p>Personal data is, in simpler terms, any information about you that enables you to be identified. Personal data covers obvious information such as your name and contact details, but it also covers less obvious information such as identification numbers, electronic location data, and other online identifiers.</p>',
                'is_displayed' => true,
                'order'        => 4,
            ],
            [
                'title'        => 'What are my rights?',
                'content'      => '
                <p>Under the GDPR, you have the following rights:</p>
<ol>
<li>The right to be informed about our collection and use of your personal data. This Privacy Policy should tell you everything you need to know, but you can always contact us to find out more.</li>
<li>The right to access the personal data we hold about you. Part 13 will tell you how to do this.</li>
<li>The right to have your personal data rectified if any of your personal data held by us is inaccurate or incomplete. Please contact us using the details in Part 15 to find out more.</li>
<li>The right to be forgotten, i.e. the right to ask us to delete or otherwise dispose of any of your personal data that we have. Please contact us using the details in Part 15 to find out more.</li>
<li>The right to restrict the processing of your personal data.</li>
<li>The right to object to us using your personal data for a particular purpose or purposes.</li>
<li>The right to data portability. This means that, if you have provided personal data to us directly, we are using it with your consent or for the performance of a contract, and that data is processed using automated means, you can ask us for a copy of that personal data to re-use with another service or business in many cases.</li>
<li>Rights relating to automated decision-making and profiling.</li>
</ol>
',
                'is_displayed' => true,
                'order'        => 5,
            ],
            [
                'title'        => 'What data do you collect?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 6,
            ],
            [
                'title'        => 'How do you use my personal data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 7,
            ],
            [
                'title'        => 'How long do you store my data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 8,
            ],
            [
                'title'        => 'How and where do you store my data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 9,
            ],
            [
                'title'        => 'Do you share my personal data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 10,
            ],
            [
                'title'        => 'How can I control my personal data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 11,
            ],
            [
                'title'        => 'How can I access my personal data?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 12,
            ],
            [
                'title'        => 'How do you use cookies?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 13,
            ],
            [
                'title'        => 'How do I contact you?',
                'content'      => '',
                'is_displayed' => true,
                'order'        => 14,
            ],
        ];
    }
}
