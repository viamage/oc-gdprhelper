<?php namespace Viamage\GDPRHelper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateScriptRulesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_gdprhelper_script_rules', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->text('snippet')->nullable();
            $table->boolean('is_displayed')->default(false);
            $table->boolean('is_required')->default(false);
            $table->integer('order')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_gdprhelper_script_rules');
    }
}
