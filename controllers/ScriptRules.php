<?php namespace Viamage\GDPRHelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\GDPRHelper\Models\ScriptRule;

/**
 * Cookie Rules Back-end Controller
 */
class ScriptRules extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.GDPRHelper', 'gdprhelper', 'scriptrules');
    }

    /**
     * Deleted checked scriptrules.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && \is_array($checkedIds) && \count($checkedIds)) {

            foreach ($checkedIds as $scriptruleId) {
                if (!$scriptrule = ScriptRule::find($scriptruleId)) {
                    continue;
                }
                $scriptrule->delete();
            }

            Flash::success(Lang::get('viamage.gdprhelper::lang.scriptrules.delete_selected_success'));
        } else {
            Flash::error(Lang::get('viamage.gdprhelper::lang.scriptrules.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
