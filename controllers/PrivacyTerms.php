<?php namespace Viamage\GDPRHelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\GDPRHelper\Models\PrivacyTerm;

/**
 * Privacy Terms Back-end Controller
 */
class PrivacyTerms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.GDPRHelper', 'gdprhelper', 'privacyterms');
    }

    /**
     * Deleted checked privacyterms.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && \is_array($checkedIds) && \count($checkedIds)) {

            foreach ($checkedIds as $privacytermId) {
                if (!$privacyterm = PrivacyTerm::find($privacytermId)) {
                    continue;
                }
                $privacyterm->delete();
            }

            Flash::success(Lang::get('viamage.gdprhelper::lang.privacyterms.delete_selected_success'));
        } else {
            Flash::error(Lang::get('viamage.gdprhelper::lang.privacyterms.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
