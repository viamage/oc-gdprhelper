<?php

return [
    'plugin'       => [
        'name'        => 'GDPR Helper',
        'description' => 'Plugin to help you manage with GDPR',
    ],
    'permissions'  => [
        'some_permission' => 'Permission example',
        'privacyterms'    => 'Privacy Terms',
        'scriptrules'     => 'Script Rules',
    ],
    'privacyterm'  => [
        'new'           => 'New Privacy Term',
        'label'         => 'Privacy Term',
        'create_title'  => 'Create Privacy Term',
        'update_title'  => 'Edit Privacy Term',
        'preview_title' => 'Preview Privacy Term',
        'list_title'    => 'Manage Privacy Terms',
        'reorder'       => 'Reorder',
        'reorder_title' => 'Reorder Privacy Terms',
    ],
    'privacyterms' => [
        'delete_selected_confirm' => 'Delete the selected Privacy Terms?',
        'menu_label'              => 'Privacy Terms',
        'return_to_list'          => 'Return to Privacy Terms',
        'delete_confirm'          => 'Do you really want to delete this Privacy Term?',
        'delete_selected_success' => 'Successfully deleted the selected Privacy Terms.',
        'delete_selected_empty'   => 'There are no selected Privacy Terms to delete.',
    ],
    'components'   => [
        'privacyterms'  => [
            'name'        => 'Privacy Policy',
            'description' => 'Displays Privacy Policy',
        ],
        'cookiewarning' => [
            'name'        => 'Cookie Warning',
            'description' => 'Displays opt-in cookie warning',
        ],
    ],
    'scriptrule'   => [
        'new'           => 'New Script Rule',
        'label'         => 'Script Rule',
        'create_title'  => 'Create Script Rule',
        'update_title'  => 'Edit Script Rule',
        'preview_title' => 'Preview Script Rule',
        'list_title'    => 'Manage Script Rules',
        'reorder'       => 'Reorder',

    ],
    'scriptrules'  => [
        'delete_selected_confirm' => 'Delete the selected Script Rules?',
        'menu_label'              => 'Script Rules',
        'return_to_list'          => 'Return to Script Rules',
        'delete_confirm'          => 'Do you really want to delete this Script Rule?',
        'delete_selected_success' => 'Successfully deleted the selected Script Rules.',
        'delete_selected_empty'   => 'There are no selected Script Rules to delete.',
        'reorder_title'           => 'Reorder Script Rules',
    ],
    'models'       => [
        'privacyterm' => [
            'label'        => 'Label',
            'content'      => 'Content',
            'is_displayed' => 'Is displayed',
            'order'        => 'Order',
            'id'           => 'ID',
            'title'        => 'Title',
            'is_filled'    => 'Is filled',
        ],
        'scriptrule'  => [
            'name'             => 'Name',
            'slug'             => 'Slug',
            'snippet'          => 'Snippet',
            'is_displayed'     => 'Is displayed',
            'is_required'      => 'Is required',
            'description'      => 'Description',
            'description_desc' => 'This will appear in cookie warning banner near checkbox',
            'snippet_desc'     => 'This will be loaded with ajax on consent',
            'order'            => 'Order',
        ],
    ],
];