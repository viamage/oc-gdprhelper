<?php namespace Viamage\GDPRHelper\Models;

use Model;

/**
 * ScriptRule Model
 */
class ScriptRule extends Model
{
    use \October\Rain\Database\Traits\Sortable;

    const SORT_ORDER = 'order';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_gdprhelper_script_rules';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * Cache clear
     */
    public function afterSave(){
        \Cache::forget('vm_script_rules');
    }

    /**
     * Cache clear
     */
    public function afterDelete(){
        \Cache::forget('vm_script_rules');
    }


    /***************************************************
     * GETTERS
     */

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return null|string
     */
    public function getSnippet(): ?string
    {
        return $this->snippet;
    }

    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        return (bool)$this->is_displayed;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return (bool)$this->is_required;
    }
}
