<?php namespace Viamage\GDPRHelper\Models;

use Model;

/**
 * PrivacyTerm Model
 */
class PrivacyTerm extends Model
{
    use \October\Rain\Database\Traits\Sortable;

    const SORT_ORDER = 'order';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_gdprhelper_privacy_terms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * Cache clear
     */
    public function afterSave()
    {
        \Cache::forget('vm_privacy_terms');
    }

    /**
     * Cache clear
     */
    public function afterDelete()
    {
        \Cache::forget('vm_privacy_terms');
    }


    /***************************************************
     * GETTERS
     */

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        return (bool)$this->is_displayed;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }
}
